import time
import threading

try:
    from urlparse import urlparse, urljoin
except ImportError:
    from urllib.parse import urlparse, urljoin

import lxml
from lxml import html

from utils import preferred_clock


class TokenBucketRateLimiter():
    """Simple, thread-safe token bucket ratelimiter
    based on https://stackoverflow.com/a/668327/4674587"""
    def __init__(self, rate, per, use_subdomain=False):
        if not isinstance(rate, (float, int)) or rate <= 0:
            raise ValueError('Rate should be >0')
        if not isinstance(per, (float, int)) or per <= 0:
            raise ValueError('Period should be >0 seconds')

        self.rate = int(rate)  # rate (of calls)
        self.per = float(per)  # per time unit (seconds)
        self.use_subdomain = use_subdomain

        self.lock = threading.Lock()
        self.dict = {}
        self.allowance = rate
        self.last_check = preferred_clock()

    def update_allowance(self):
        current = preferred_clock()
        time_passed = current - self.last_check
        self.last_check = current
        self.allowance += time_passed * (self.rate / self.per)

    def call(self):
        with self.lock:
            self.update_allowance()
            if self.allowance > self.rate:
                # prevent allowance from growing past the rate
                self.allowance = self.rate

            # print ('allowance', self.allowance)
            if self.allowance < 1.0:
                time.sleep(1.0 - self.allowance)
                self.update_allowance()

            self.allowance -= 1.0

    def __enter__(self):
        return self.call()

    def __exit__(self, *args):
        return


def get_domain_name(url, scheme=True):
    parsed_uri = urlparse(url)

    if scheme:
        # https://www.example.com/category/shoes -> https://www.example.com/
        return '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    else:
        # https://www.example.com/category/shoes -> www.example.com
        return '{uri.netloc}'.format(uri=parsed_uri)


def extract_urls(request):
    domain_name = get_domain_name(request.url, scheme=True)
    try:
        doc = html.fromstring(request.content)
    except lxml.etree.ParserError:
        # lxml.etree.ParserError: Document is empty
        return []

    urls = doc.xpath("//a/@href")

    # convert relative path to absolute path e.g. "/page.html" to "http://www.example.com/page.html"
    urls = [
        url if (url.startswith('http://') or url.startswith('https://')) else urljoin(domain_name, url) for url in urls
    ]
    return urls


def remove_external_urls(site_url, urls):
    # Filter out urls not belonging to the same root url
    # e.g. remove if the root url is "http://www.example.com/" and the url to compare is "https://www.iana.org/domains/example"
    domain_name_no_scheme = get_domain_name(site_url, scheme=False)
    urls = [url for url in urls if get_domain_name(url, scheme=False) == domain_name_no_scheme]
    return urls
