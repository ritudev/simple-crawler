import threading
import time
import random
from collections import OrderedDict
import urllib.robotparser

try:
    import Queue as queue
    from urlparse import urlparse, urljoin
except ImportError:
    import queue
    from urllib.parse import urlparse, urljoin

import requests
from lxml import html

from utils import preferred_clock, get_valid_filename
from http_utils import TokenBucketRateLimiter, get_domain_name, extract_urls, remove_external_urls


class Crawler():
    def __init__(self, site_url=None, use_threads=False, threads=1, rate_limiter=None, max_request_attempts=3):
        if not site_url:
            raise ValueError('Please specify the root url')

        self.site_url = site_url
        self.use_threads = use_threads
        self.threads = threads
        self._list_of_threads = []
        self.rate_limiter = rate_limiter or TokenBucketRateLimiter(rate=2, per=1)  # default: 2 requests per 1 second

        self.lock = threading.Lock()
        self.io_lock = threading.Lock()
        self.url_results = OrderedDict()
        self.url_queue = queue.Queue()
        self.session = requests.Session()
        self.max_request_attempts = max_request_attempts or 3

        self.filename = get_valid_filename(get_domain_name(site_url, scheme=False)) + '.txt'
        with open(self.filename, 'w'):
            pass  # clear the contents

    def crawl_robotstxt(self):
        self.rp = urllib.robotparser.RobotFileParser()
        robots_url = urljoin(get_domain_name(self.site_url, scheme=True), 'robots.txt')
        print('robots_url', robots_url)
        self.rp.set_url(robots_url)
        self.rp.read()

    def send_request(self, url, timeout=60.05):
        for retry_idx in range(self.max_request_attempts):
            try:
                with self.rate_limiter:
                    request = self.session.get(url, timeout=timeout)

                return request
            except requests.exceptions.RequestException as e:
                print('Request error: {}'.format(e))
                continue
            except Exception:
                raise

    def start(self, domains=None):
        self.crawl_robotstxt()
        print('Saving to: {} | Rate limit: {} requests per {}s'.format(crawler.filename, self.rate_limiter.rate, self.rate_limiter.per))
        self.url_queue.put(site_url)

        if not self.use_threads:
            self.worker()
        else:
            for x in range(self.threads):
                t = threading.Thread(target=self.worker)
                t.daemon = True
                t.start()
                self._list_of_threads.append(t)

            try:
                while any(t.is_alive() for t in self._list_of_threads):
                    time.sleep(1)
            except KeyboardInterrupt:
                with self.io_lock:
                    raise

    def worker(self):
        while True:
            try:
                url = self.url_queue.get(timeout=10)
            except queue.Empty:
                print('Empty queue')
                url = None

            if url is None:
                return  # no more urls, we are done

            if not self.rp.can_fetch('*', url):
                # robots.txt disallows crawling
                print('robots.txt skip', url)
                continue

            try:
                if self.url_results[url]['crawled']:
                    # print ('skip', url)
                    continue
            except KeyError:
                pass

            request = self.send_request(url=url)
            hrefs = extract_urls(request=request)
            hrefs = remove_external_urls(site_url=self.site_url, urls=hrefs)

            with self.lock:
                hrefs = [href for href in hrefs if href not in self.url_results]

                for href in hrefs:
                    # print ('href', href)
                    self.url_results[href] = {'parent_url': url, 'crawled': False}
                    self.url_queue.put(href)

            try:
                self.url_results[url].update({
                    'crawled': True,
                    'status_code': request.status_code,
                })
            except KeyError:
                # Root site url
                self.url_results[url] = {
                    'parent_url': 'root',
                    'crawled': True,
                    'status_code': request.status_code,
                }

            print(
                '{parent_url} --> {url} | Unique hrefs: {hrefs_len}'.format(
                    done=self.url_queue.qsize(),
                    parent_url=self.url_results[url]['parent_url'],
                    url=url,
                    hrefs_len=len(hrefs),
                )
            )
            self.save_results(url)

    def save_results(self, url):
        with self.io_lock:
            with open(self.filename, 'a') as f:
                f.write('{}\n'.format(url))


if __name__ == '__main__':
    # SETTINGS START
    site_url = 'https://www.youtube.com/'
    # site_url = 'http://www.example.com/'
    # site_url = 'https://httpbin.davecheney.com/'

    use_threads = True
    threads = 2
    rate_limiter = TokenBucketRateLimiter(rate=2, per=1)  # 2 requests per 1 second
    max_request_attempts = 3
    # SETTINGS END

    crawler = Crawler(
        site_url=site_url,
        use_threads=use_threads,
        threads=threads,
        rate_limiter=rate_limiter,
        max_request_attempts=max_request_attempts,
    )
    crawler.start()

    print('Done, check {}'.format(crawler.filename))
